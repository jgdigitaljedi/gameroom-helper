import React from 'react';
import { Box } from 'theme-ui';
import FilterBar from '@components/FilterBar';
import GamesSection from '@components/GamesSection';
import ServicesBar from '@components/ServicesBar';
import ScrollToTop from './components/ScrollToTop';

function App() {
  return (
    <Box sx={{ p: 4 }}>
      <ServicesBar />
      <FilterBar />
      <GamesSection />
      <ScrollToTop />
    </Box>
  );
}

export default App;
