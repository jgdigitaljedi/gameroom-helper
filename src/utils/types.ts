export interface TGame {
  id: number;
  first_release_date: string | null;
  total_rating: number | null;
  esrb: string | null;
  videos: string[];
  image: string | null;
  description: string | null;
  story: string | null;
  player_perspectives: string[];
  multiplayer_modes: {
    offlinemax: number;
    offlinecoopmax: number;
    splitscreen: boolean;
  };
  maxMultiplayer: number | string;
  manual: boolean;
  compilation: boolean;
  compilationGamesIds: number[];
  gamesService: {
    xbGold: boolean;
    xbPass: boolean;
    psPlus: boolean;
    primeFree: boolean;
  };
  consoleName: string;
  consoleId: number;
  condition: string;
  case: string;
  pricePaid: number;
  physical: any;
  cib: any;
  datePurchased: string;
  howAcquired: string;
  notes: string;
  createdAt: string;
  updatedAt: string;
  _id: string;
  name: string;
  extraData: string[];
  extraDataFull: string[];
  genres: string[];
  location: string;
  handheld: boolean;
  genresDisplay: string;
  vr: {
    vrOnly: boolean;
    vrCompatible: boolean;
  };
  caseType: string;
}

export interface TPlatform {
  alternative_name?: string;
  box: boolean;
  category: string;
  company: string;
  condition: string;
  connectivity: string | null;
  cpu: string | null;
  createdAt: string;
  datePurchased: string | null;
  generation?: number;
  ghostConsole: boolean;
  howAcquired: string | null;
  id: number;
  lastUpdated: string;
  logo: string | null;
  manual: boolean;
  media: string;
  memory: string | null;
  mods: string;
  name: string;
  newDatePurchased: Date | null;
  notes: string;
  os: string | null;
  output: string | null;
  platform_logo: {
    id: number;
    url: string | null;
  };
  priceCharting?: {
    consoleName: string | null;
    id: string;
    price: number | null;
    name: string | null;
    case: string | null;
    lastUpdated: string | null;
  };
  pricePaid: number | null;
  releaseDate: {
    date: string;
    region: string;
  };
  resolutions: string;
  storage: string;
  summary: string;
  updatedAt: string;
  version: {
    id: number;
    name: string;
  };
  _id: string;
}

export enum FpgaEmu {
  BOTH,
  PHYSICAL,
  EMU
}

export enum Handheld {
  BOTH,
  CONSOLE,
  HANDHELD
}

export interface FilterSelections {
  multiplayer_modes: {
    offlinemax: number;
    offlinecoopmax: number;
  };
  coop: boolean;
  splitscreen: boolean;
  maxMultiplayer: number;
  esrb: string | null;
  releasedAfter: Date | null;
  releasedBefore: Date | null;
  vr: boolean;
  platform: string[];
  fpgaEmu: FpgaEmu;
  handheld: Handheld;
  name: string;
}

export enum SortCategory {
  CREATED = 'createdAt',
  RELEASED = 'first_release_date',
  RATING = 'total_rating',
  PAID = 'pricePaid'
}

export type SortDirection = 'desc' | 'asc';

export interface SortingOpts {
  direction: SortDirection;
  category: SortCategory;
}
