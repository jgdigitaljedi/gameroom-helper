import { FilterSelections, TGame } from './types';

export const parseNum = (val: string | number, min?: number): number => {
  if (typeof val === 'string') {
    const newVal = parseInt(val);
    if (min && newVal < min) {
      return min;
    }
    return newVal;
  }
  return val;
};

export const filterGamesFn = (fils: FilterSelections, games: TGame[]) => {
  return games.filter((game: TGame) => {
    console.log('fils', fils.name);
    const maxMult = parseNum(game.maxMultiplayer, 1);
    if (
      (fils.maxMultiplayer && maxMult > 1 && !game.maxMultiplayer) ||
      maxMult < fils.maxMultiplayer
    ) {
      return false;
    }
    if (fils.splitscreen && !game.multiplayer_modes.splitscreen) {
      return false;
    }
    if (!!fils.name) {
      return game.name.toLowerCase().includes(fils.name.toLowerCase());
    }
    return true;
  });
};
