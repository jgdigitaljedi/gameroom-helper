import React, { createContext, useEffect, useState } from 'react';
import { TGame, TPlatform } from './types';
import games from './data/games.json';
import platforms from './data/consoles.json';

interface DataContextProps {
  games: TGame[];
  platforms: TPlatform[];
  filteredGames: TGame[];
  truncatedGames: TGame[];
  onFilterChange: (games: TGame[]) => void;
  onSortChange: (games: TGame[]) => void;
  loadMoreGames: () => void;
}

const defaultData = {
  games: [],
  platforms: [],
  filteredGames: [],
  truncatedGames: [],
  onFilterChange: () => {},
  onSortChange: () => {},
  loadMoreGames: () => {}
};

const numToLoad = 75;

export const DataContext = createContext<DataContextProps>(defaultData);

// @ts-ignore
export const DataContextProvider = ({ children }) => {
  const [filteredGames, setFilteredGames] = useState<TGame[]>(defaultData.filteredGames);
  const [truncatedGames, setTruncatedGames] = useState<TGame[]>(defaultData.games);
  const [currentNumLoaded, setCurrentNumLoaded] = useState<number>(numToLoad);

  useEffect(() => {
    setFilteredGames(games as TGame[]);
    setTruncatedGames((games as TGame[]).slice(0, currentNumLoaded));
    console.log('running again', filteredGames);
  }, [games]);

  const loadMoreGames = (): void => {
    if (filteredGames?.length) {
      const newAmountLoaded = currentNumLoaded + numToLoad;
      setCurrentNumLoaded(newAmountLoaded);
      setTruncatedGames([...filteredGames].slice(0, newAmountLoaded));
      console.log('data trunc', filteredGames);
    } else {
      console.log('filtered IN THE ELSE', filteredGames);
    }
  };

  const onFilterChange = (filtered: TGame[]): void => {
    setFilteredGames(filtered);
    setTruncatedGames([...filtered].slice(0, currentNumLoaded));
  };

  const onSortChange = (sorted: TGame[]): void => {
    setFilteredGames(sorted);
    setTruncatedGames([...sorted].slice(0, currentNumLoaded));
  };

  return (
    <DataContext.Provider
      value={{
        // @ts-ignore
        games,
        // @ts-ignore
        platforms,
        filteredGames,
        truncatedGames,
        onFilterChange,
        onSortChange,
        loadMoreGames
      }}
    >
      {children}
    </DataContext.Provider>
  );
};
