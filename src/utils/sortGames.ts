import { DateTime } from 'luxon';
import { SortCategory, SortDirection, TGame } from './types';

const catToKey = {
  [SortCategory.RELEASED]: 'first_release_date',
  [SortCategory.CREATED]: 'createdAt',
  [SortCategory.PAID]: 'pricePaid',
  [SortCategory.RATING]: 'total_rating'
};

const datetimeFormats = {
  [SortCategory.RELEASED]: 'MM/dd/yyyy',
  [SortCategory.CREATED]: 'MM/dd/yyyy t'
};

const dateSort = (dir: SortDirection, cat: SortCategory, games: TGame[]): TGame[] => {
  const prop = catToKey[cat];
  // @ts-ignore
  const tForm = datetimeFormats[cat];
  const sorted = games.sort((a: TGame, b: TGame): number => {
    // @ts-ignore
    const aMill = a[prop] ? DateTime.fromFormat(a[prop], tForm).toMillis() : null;
    // @ts-ignore
    const bMill = b[prop] ? DateTime.fromFormat(b[prop], tForm).toMillis() : null;
    if (!aMill) {
      return 1;
    }
    if (!bMill) {
      return 1;
    }
    return aMill - bMill;
  });
  return dir === 'asc' ? sorted.reverse() : sorted;
};

export const sortGames = (dir: SortDirection, cat: SortCategory, games: TGame[]) => {
  if (cat === SortCategory.CREATED || cat === SortCategory.RELEASED) {
    return dateSort(dir, cat, games);
  }
  const prop = catToKey[cat];
  const sorted = games.sort((a: TGame, b: TGame) => {
    // @ts-ignore
    return a[prop] - b[prop];
  });
  return dir === 'asc' ? sorted.reverse() : sorted;
};
