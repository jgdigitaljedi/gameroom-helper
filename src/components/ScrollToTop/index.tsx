import React, { useState } from 'react';
import { FaArrowUp } from 'react-icons/fa';
import { Button } from 'theme-ui';

const ScrollToTop = () => {
  const [visible, setVisible] = useState(false);

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 300) {
      setVisible(true);
    } else if (scrolled <= 300) {
      setVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  };

  window.addEventListener('scroll', toggleVisible);

  return (
    <Button
      variant="primary"
      sx={{
        borderRadius: '50%',
        width: '3rem',
        height: '3rem',
        bottom: '2rem',
        right: '2rem',
        zIndex: 10,
        position: 'fixed',
        display: visible ? 'inline' : 'none',
        boxShadow: 'card'
      }}
    >
      <FaArrowUp
        onClick={scrollToTop}
        style={{ marginTop: '4px', fontSize: '1.2rem', marginLeft: '-2px' }}
      />
    </Button>
  );
};

export default ScrollToTop;
