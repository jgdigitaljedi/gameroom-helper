import React, { ReactNode } from 'react';
import { Box, Close, Flex, Grid, Heading, ThemeUIStyleObject } from 'theme-ui';
import { keyframes } from '@emotion/react';

interface ModalProps {
  isOpen: boolean;
  title: string;
  toggle: () => void;
  children: ReactNode;
}

const fadeIn = keyframes({ from: { opacity: 0 }, to: { opacity: 1 } });

const modalBackdropStyle: ThemeUIStyleObject = {
  position: 'fixed',
  top: 0,
  left: 0,
  background: 'rgba(0, 0, 0, .5)',
  height: '100vh',
  width: '100%',
  zIndex: 10,
  animation: `${fadeIn} .3s`
};

const modalWrapperStyle: ThemeUIStyleObject = {
  position: 'fixed',
  zIndex: 20,
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bg: 'bgLighter',
  boxShadow: 'elevated',
  p: 2,
  animation: `${fadeIn} .3s`,
  borderRadius: 'default',
  width: '100%',
  maxWidth: '128rem'
};

const Modal: React.FC<ModalProps> = ({ isOpen, title, toggle, children }) => {
  return (
    <>
      {isOpen ? (
        <>
          <Box sx={modalBackdropStyle} onClick={toggle} />
          <Box sx={modalWrapperStyle} onClick={(e: any) => e.stopPropagation()}>
            <Grid columns={3}>
              <Box />
              <Heading as="h1">{title}</Heading>
              <Close
                onClick={toggle}
                sx={{
                  cursor: 'pointer',
                  justifySelf: 'end'
                }}
              />
            </Grid>
            <Flex sx={{ p: 3 }}>{children}</Flex>
          </Box>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default Modal;
