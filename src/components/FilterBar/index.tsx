import React, { ChangeEvent, useCallback, useContext, useMemo, useState } from 'react';
import {
  FilterSelections,
  FpgaEmu,
  Handheld,
  SortCategory,
  SortDirection,
  SortingOpts
} from '@utils/types';
import { Box, Card, Field, Grid, Label, Select, Switch, Text } from 'theme-ui';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import FilterSection from './FilterSection';
import { DataContext } from '@utils/DataContext';
import { filterGamesFn, parseNum } from '@utils/filterGames';
import { debounce } from 'lodash';
import { sortGames } from '@/utils/sortGames';

type SortWhich = 'direction' | 'category';

const baseFilters: FilterSelections = {
  multiplayer_modes: {
    offlinemax: 0,
    offlinecoopmax: 0
  },
  coop: false,
  splitscreen: false,
  maxMultiplayer: 1,
  esrb: null,
  releasedAfter: null,
  releasedBefore: null,
  vr: false,
  platform: [],
  fpgaEmu: FpgaEmu.BOTH,
  handheld: Handheld.BOTH,
  name: ''
};

const baseSorting: SortingOpts = {
  direction: 'desc',
  category: SortCategory.CREATED
};

const FilterBar: React.FC = () => {
  const { games, onFilterChange } = useContext(DataContext);
  const [currentFilters, setCurrentFilters] = useState<FilterSelections>(baseFilters);
  const [currentSorting, setCurrentSorting] = useState<SortingOpts>(baseSorting);

  const filterGames = useCallback((): void => {
    const filtered = filterGamesFn(currentFilters, games);
    onFilterChange(filtered);
  }, [currentFilters]);

  const onInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>,
    which: keyof typeof baseFilters
  ): void => {
    console.log('e', e.target.value);
    const val = which === 'maxMultiplayer' ? parseNum(e.target.value, 1) : e.target.value;
    const fils = { ...currentFilters, [which]: val };
    setCurrentFilters(fils);
    filterGames();
  };

  const debouncedFilter = useCallback(debounce(filterGames, 400), [filterGames]);

  const debouncedInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>,
    which: keyof typeof baseFilters
  ) => {
    const fils = { ...currentFilters, [which]: e.target.value };
    setCurrentFilters(fils);
    console.log('currentFilters', currentFilters);
    debouncedFilter();
  };

  const onDateChange = (date: any, which: keyof typeof baseFilters): void => {
    console.log('date', date);
    const fils = { ...currentFilters, [which]: date };
    setCurrentFilters(fils);
    filterGames();
  };

  const onSwitchChange = (which: keyof typeof baseFilters): void => {
    const fils = { ...currentFilters, [which]: !currentFilters[which] };
    setCurrentFilters(fils);
    filterGames();
  };

  const onSortSelectChange = (e: ChangeEvent<HTMLSelectElement>, which: SortWhich): void => {
    const val = e.target?.value;
    if (which === 'direction') {
      setCurrentSorting({ ...currentSorting, direction: val as SortDirection });
    } else {
      setCurrentSorting({ ...currentSorting, category: val as SortCategory });
    }
    const sorted = sortGames(currentSorting.direction, currentSorting.category, games);
    onFilterChange(sorted);
  };

  const filterBaseStyle = { my: 2, maxWidth: '10rem' };

  return (
    <Card variant="recent" sx={{ p: 2 }}>
      <Grid gap={2} columns={[4, '1fr 1fr 1fr 1fr']}>
        <FilterSection title="Player Options">
          <Box sx={filterBaseStyle}>
            <Field
              label="Num players"
              name="Num players"
              value={currentFilters.maxMultiplayer}
              type="number"
              onChange={(e: any) => onInputChange(e, 'maxMultiplayer')}
              max={8}
              min={1}
            />
          </Box>
          <Box sx={filterBaseStyle}>
            <Switch
              checked={currentFilters.splitscreen}
              label="Splitscreen"
              name="splitscreen"
              onChange={() => onSwitchChange('splitscreen')}
            />
          </Box>
          <Box sx={filterBaseStyle}>
            <Switch
              checked={currentFilters.coop}
              label="Co-op"
              name="co-op"
              onChange={() => onSwitchChange('coop')}
            />
          </Box>
        </FilterSection>
        <FilterSection title="Game data">
          <Box sx={filterBaseStyle}>
            <DatePicker
              selected={baseFilters.releasedBefore}
              onChange={(date: any) => onDateChange(date, 'releasedBefore')}
            />
          </Box>
          <Field
            label="Name"
            name="name"
            value={currentFilters.name}
            onChange={(e: any) => debouncedInputChange(e, 'name')}
            sx={{ mt: 2 }}
          />
        </FilterSection>
        <FilterSection title="Platform options">
          <Switch
            checked={currentFilters.vr}
            label="VR"
            name="vr"
            onChange={() => onSwitchChange('vr')}
          />
          <Select
            value={currentFilters.handheld}
            sx={{ mt: 2 }}
            onChange={(e) => onInputChange(e, 'handheld')}
          >
            <option value={Handheld.BOTH}>Handhelds and consoles</option>
            <option value={Handheld.HANDHELD}>Handhelds only</option>
            <option value={Handheld.CONSOLE}>Consoles only</option>
          </Select>
          <Switch label="Include emulation/flashcarts" name="emu" sx={{ mt: 2 }} />
        </FilterSection>
        <FilterSection title="Sorting">
          <Label sx={{ display: 'flex', alignItems: 'center' }}>
            <Text>Sort by</Text>
            <Select
              value={currentSorting.category}
              onChange={(e) => onSortSelectChange(e, 'category')}
              sx={{ ml: 2 }}
            >
              <option value={SortCategory.CREATED}>Date added</option>
              <option value={SortCategory.RELEASED}>Release date</option>
              <option value={SortCategory.RATING}>Rating</option>
              <option value={SortCategory.PAID}>Price paid</option>
            </Select>
          </Label>
          <Label sx={{ display: 'flex', alignItems: 'center', mt: 2 }}>
            <Text>Direction</Text>
            <Select
              value={currentSorting.direction}
              onChange={(e) => onSortSelectChange(e, 'direction')}
              sx={{ ml: 2 }}
            >
              <option value="desc">desc</option>
              <option value="asc">asc</option>
            </Select>
          </Label>
        </FilterSection>
      </Grid>
    </Card>
  );
};

export default FilterBar;
