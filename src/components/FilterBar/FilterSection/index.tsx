import React, { ReactNode } from 'react';
import { Flex, Heading, ThemeUIStyleObject } from 'theme-ui';

interface FilterSectionProps {
  title: string;
  sxProps?: ThemeUIStyleObject;
  children: ReactNode;
}

const FilterSection: React.FC<FilterSectionProps> = ({ title, sxProps, children }) => {
  return (
    <Flex
      sx={{
        borderRadius: 'default',
        borderColor: 'uglyGray',
        borderWidth: '1px',
        borderStyle: 'solid',
        p: 3,
        flexDirection: 'column',
        width: 'auto',
        maxWidth: '100%',
        ...(sxProps || {})
      }}
    >
      <Heading as="h3" sx={{ color: 'secondary', mb: 2 }}>
        {title}
      </Heading>
      {children}
    </Flex>
  );
};

export default FilterSection;
