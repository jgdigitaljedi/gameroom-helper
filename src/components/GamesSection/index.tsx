import React, { useContext, useEffect, useRef, useState } from 'react';
import { Box, Flex } from 'theme-ui';
import GameCard from './GameCard';
import { DataContext } from '@utils/DataContext';
import Modal from '../Modal';
import { TGame } from '@utils/types';
import GameDialog from './GameDialog';

const GamesSection: React.FC = () => {
  const { filteredGames, truncatedGames, loadMoreGames } = useContext(DataContext);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [clickedGame, setClicked] = useState<TGame | null>(null);
  const bottom = useRef(null);

  const onGameCardClicked = (game: TGame): void => {
    setClicked(game);
    setIsModalOpen(true);
  };

  const onCloseClicked = (): void => {
    setClicked(null);
    setIsModalOpen(false);
  };

  useEffect(() => {
    console.log('truncated', truncatedGames);
    const observer = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting && filteredGames.length > truncatedGames.length) {
        loadMoreGames();
      }
    });
    if (bottom?.current) {
      observer.observe(bottom.current);
    }
  }, [loadMoreGames, filteredGames, truncatedGames]);

  return (
    <Box>
      <Box sx={{ mb: 3, fontWeight: 'bold', textAlign: 'center', fontSize: '1.5rem' }}>
        {filteredGames.length} games
      </Box>
      <Flex sx={{ flexWrap: 'wrap', justifyContent: 'center' }}>
        {truncatedGames.map((game) => {
          return (
            <Box onClick={() => onGameCardClicked(game)} key={game._id}>
              <GameCard game={game} />
            </Box>
          );
        })}
      </Flex>
      <Box ref={bottom} />
      <Modal isOpen={isModalOpen} title={clickedGame?.name || ''} toggle={onCloseClicked}>
        <GameDialog game={clickedGame as TGame} />
      </Modal>
    </Box>
  );
};

export default GamesSection;
