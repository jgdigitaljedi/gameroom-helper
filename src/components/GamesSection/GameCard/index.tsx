import { TGame } from '@/utils/types';
import React from 'react';
import { Box, Card, Flex, Image, Text } from 'theme-ui';
import { FaStar } from 'react-icons/fa';
import { MdPerson } from 'react-icons/md';

interface GameCardProps {
  game: TGame;
}

const GameCard: React.FC<GameCardProps> = ({ game }) => {
  return (
    <Card
      sx={{
        maxWidth: '15rem',
        width: '15rem',
        p: 0,
        backgroundColor: 'background',
        height: '20rem',
        overflow: 'hidden',
        borderColor: 'primary',
        borderStyle: 'solid',
        borderWidth: '1px',
        cursor: 'pointer',
        position: 'relative'
      }}
    >
      <Box
        sx={{
          p: 2,
          fontWeight: 'bold',
          backgroundColor: 'bgLighter',
          height: '4rem',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          display: 'block'
        }}
      >
        {game.name}
      </Box>
      {game.image ? (
        <Image src={game.image} sx={{ objectFit: 'contain', maxHeight: '100%' }} loading="lazy" />
      ) : (
        <></>
      )}
      <Flex
        sx={{
          position: 'absolute',
          zIndex: 10,
          bottom: 0,
          width: '100%',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.7)'
        }}
      >
        <Flex sx={{ alignItems: 'center' }}>
          <FaStar />
          <Text sx={{ ml: 1 }}>{game.total_rating}</Text>
        </Flex>
        <Flex sx={{ alignItems: 'center' }}>
          <MdPerson />
          <Text sx={{ ml: 1 }}>{game.maxMultiplayer}</Text>
        </Flex>
      </Flex>
    </Card>
  );
};

export default GameCard;
