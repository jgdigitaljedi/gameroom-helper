import React from 'react';
import { TGame } from '@utils/types';
import { Flex, Heading, Image, Text } from 'theme-ui';

interface GameDialogProps {
  game: TGame;
}

const GameDialog: React.FC<GameDialogProps> = ({ game }) => {
  return (
    <Flex sx={{ flexDirection: 'column', width: '100%' }}>
      {game.description && (
        <Flex sx={{ flexDirection: 'column' }}>
          <Heading as={'h3'}>Description</Heading>
          <Text>{game.description}</Text>
        </Flex>
      )}
      {game.image && <Image src={game.image} alt={game.name} sx={{ maxWidth: '16rem' }} />}
    </Flex>
  );
};

export default GameDialog;
