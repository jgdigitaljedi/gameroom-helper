import React, { useEffect, useState } from 'react';
import { Card, Flex, Switch } from 'theme-ui';
import FilterSection from '../FilterBar/FilterSection';

type ServiceOpts = 'gold' | 'plus' | 'pass' | 'switch';

const ServicesBar: React.FC = () => {
  const [gold, setGold] = useState(false);
  const [pass, setPass] = useState(false);
  const [plus, setPlus] = useState(false);
  const [nSwitch, setNSwitch] = useState(false);

  const onChangeSet = (
    which: ServiceOpts,
    currentVal: boolean,
    stateCb: (val: boolean) => void
  ): void => {
    const newVal = !currentVal;
    localStorage.setItem(which, JSON.stringify(newVal));
    stateCb(newVal);
  };

  useEffect(() => {
    const goldStor = localStorage.getItem('gold') || null;
    const plusStor = localStorage.getItem('plus') || null;
    const passStor = localStorage.getItem('pass') || null;
    const switchStor = localStorage.getItem('switch') || null;

    if (!!goldStor) {
      setGold(JSON.parse(goldStor));
    }
    if (!!plusStor) {
      setPlus(JSON.parse(plusStor));
    }
    if (!!passStor) {
      setPass(JSON.parse(passStor));
    }
    if (!!switchStor) {
      setNSwitch(JSON.parse(switchStor));
    }
  }, []);

  return (
    <Card variant="recent" sx={{ p: 2, mb: 2 }}>
      <FilterSection title="Current services">
        <Flex sx={{ justifyContent: 'space-evenly' }}>
          <Switch
            label="Xbox Gold"
            checked={gold}
            onChange={() => onChangeSet('gold', gold, setGold)}
          />
          <Switch
            label="Xbox Game Pass"
            checked={pass}
            onChange={() => onChangeSet('pass', pass, setPass)}
          />
          <Switch
            label="PlayStation Plus"
            checked={plus}
            onChange={() => onChangeSet('plus', plus, setPlus)}
          />
          <Switch
            label="Nintento Switch Online"
            checked={nSwitch}
            onChange={() => onChangeSet('switch', nSwitch, setNSwitch)}
          />
        </Flex>
      </FilterSection>
    </Card>
  );
};

export default ServicesBar;
