/** @jsxImportSource theme-ui */
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import { ThemeUIProvider } from 'theme-ui';
import theme from '../theme';
import { DataContextProvider } from '@utils/DataContext.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <DataContextProvider>
      <ThemeUIProvider theme={theme}>
        <App />
      </ThemeUIProvider>
    </DataContextProvider>
  </React.StrictMode>
);
