import type { Theme } from 'theme-ui';

// --phoneWidth: (max-width: 684px);
// --tabletWidth: (max-width: 900px);
// }

// @custom-media --phone (max-width: 684px);
// @custom-media --tablet (max-width: 900px);
export const breakpointValues = ['0px', '32em', '48em', '64em', '96em', '128em'];

const theme: Theme = {
  breakpoints: breakpointValues,
  colors: {
    text: '#fff',
    background: '#1D212C',
    bgLighter: '#242834',
    primary: '#23B0FF',
    secondary: '#EF8354',
    highlight: '#8789C0',
    muted: '#3E78B2',
    gray: '#7796CB',
    success: '#5B9279',
    error: '#EF626C',
    uglyGray: '#9E90A2'
  },
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
  shadows: {
    text: '0 1px 2px rgba(0, 0, 0, 0.25), 0 2px 4px rgba(0, 0, 0, 0.125)',
    small: '0 1px 2px rgba(0, 0, 0, 0.0625), 0 2px 4px rgba(0, 0, 0, 0.0625)',
    card: '0 4px 8px rgba(0, 0, 0, 0.125)',
    elevated: '0 1px 2px rgba(0, 0, 0, 0.0625), 0 8px 12px rgba(0, 0, 0, 0.125)'
  },
  lineHeights: {
    limit: 0.875,
    title: 1,
    heading: 1.125,
    subheading: 1.25,
    caption: 1.375,
    body: 1.5
  },
  radii: {
    small: 4,
    default: 8,
    extra: 12,
    ultra: 16,
    circle: 99999
  },
  fonts: {
    body: "'Fira Code', Monaco, Consolas, Ubuntu Mono, monospace",
    heading: 'inherit',
    monospace: "'Fira Code', Monaco, Consolas, Ubuntu Mono, monospace"
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 72],
  fontWeights: {
    body: 400,
    heading: 700,
    display: 900
  },
  buttons: {
    primary: {
      cursor: 'pointer',
      boxShadow: 'small',
      bg: 'primary',
      color: 'background'
    }
  },
  cards: {
    job: {
      p: 4,
      backgroundColor: 'bgLighter',
      boxShadow: 'card',
      color: 'text',
      borderRadius: '1rem',
      mb: 4
    },
    project: {
      backgroundColor: 'bgLighter',
      boxShadow: 'card',
      color: 'text',
      borderRadius: '1rem',
      p: 4
    },
    recent: {
      variant: 'cards.job',
      p: 0
      // maxWidth: '50%'P
    }
  },
  links: {
    normal: {
      color: 'primary',
      '&:hover': {
        color: 'secondary'
      },
      '&:visited': {
        color: 'primary'
      }
    },
    secondary: {
      textDecoration: 'none',
      borderBottomWidth: '1px',
      borderBottomStyle: 'solid',
      borderBottomColor: 'inherit',
      cursor: 'pointer',
      '&:visited': {
        color: 'primary'
      }
    }
  },
  text: {
    heading: {
      fontFamily: 'heading',
      fontWeight: 'heading',
      lineHeight: 'heading'
    },
    display: {
      variant: 'text.heading',
      fontSize: [5, 6],
      fontWeight: 'display',
      letterSpacing: '-0.03em',
      mt: 3
    }
  },
  styles: {
    html: {
      boxSizing: 'border-box'
    },

    '*, *:before, *:after': {
      boxSizing: 'inherit'
    },
    body: {
      m: 0,
      p: 0,
      fontFamily: "'Fira Code', Monaco, Consolas, Ubuntu Mono, monospace",
      fontSize: '1rem',
      lineHeight: '1.54rem',
      letterSpacing: '-0.02em',
      backgroundColor: 'background',
      color: 'text',
      textRendering: 'optimizeLegibility',
      '-webkit-font-smoothing': 'antialiased',
      fontFeatureSettings: "'liga', 'tnum', 'zero', 'ss01', 'locl'",
      fontVariantLigatures: 'contextual',
      '-webkit-overflow-scrolling': 'touch',
      '-webkit-text-size-adjust': '100%'
    },
    Container: {
      p: 3,
      maxWidth: 1024
    },
    root: {
      fontFamily: 'body',
      lineHeight: 'body',
      fontWeight: 'body'
    },
    h1: {
      variant: 'text.display'
    },
    h2: {
      variant: 'text.heading',
      fontSize: 5
    },
    h3: {
      variant: 'text.heading',
      fontSize: 4
    },
    h4: {
      variant: 'text.heading',
      fontSize: 3
    },
    h5: {
      variant: 'text.heading',
      fontSize: 2
    },
    h6: {
      variant: 'text.heading',
      fontSize: 1
    },
    a: {
      color: 'primary',
      '&:hover': {
        color: 'secondary'
      },
      '&:visited': {
        color: 'primary'
      }
    },
    pre: {
      variant: 'prism',
      fontFamily: 'monospace',
      color: 'text',
      bg: 'muted',
      overflow: 'auto',
      code: {
        color: 'inherit',
        background: 'none !important',
        m: 0,
        p: 0,
        fontSize: 'inherit',
        border: 'none'
      },
      background: 'transparent !important',
      py: 4,
      px: 3,
      m: '2.5rem 0',
      fontSize: '.95rem !important',
      borderTop: '1px solid rgba(255, 255, 255, .1)',
      borderBottom: '1px solid rgba(255, 255, 255, .1)',
      '@media (--phone)': {
        whiteSpace: 'pre-wrap',
        wordWrap: 'break-word'
      }
    },
    code: {
      fontFamily: "'Fira Code', Monaco, Consolas, Ubuntu Mono, monospace !important",
      fontFeatureSettings: 'normal',
      background: 'rgba(35, 176, 255, 0.2)',
      color: 'accent',
      p: '1px 6px',
      m: '0 2px',
      fontSize: '.95rem'
    },
    inlineCode: {
      fontFamily: 'monospace',
      color: 'secondary',
      bg: 'muted'
    },
    table: {
      width: '100%',
      my: 4,
      borderCollapse: 'separate',
      borderSpacing: 0,
      'th,td': {
        textAlign: 'left',
        py: 2,
        pr: 2,
        pl: 0,
        borderColor: 'muted',
        borderBottomStyle: 'solid'
      }
    },
    th: {
      verticalAlign: 'bottom',
      borderBottomWidth: '2px'
    },
    td: {
      verticalAlign: 'top',
      borderBottomWidth: '1px'
    },
    hr: {
      border: 0,
      borderBottom: '1px solid',
      borderColor: 'muted'
    },
    img: {
      display: 'block',
      maxWidth: '100%',
      '&.left': {
        marginRight: 'auto'
      },
      '&.center': {
        marginLeft: 'auto',
        marginRight: 'auto'
      },
      '&.right': {
        marginLeft: 'auto'
      }
    },
    p: {
      marginBottom: '20px'
    },
    blockquote: {
      borderTopWidth: '1px',
      borderTopStyle: 'solid',
      borderTopColor: 'accent',
      borderBottomWidth: '1px',
      borderBottomStyle: 'solid',
      borderBottomColor: 'accent',
      m: '2.5rem 0',
      padding: '25px',
      '@media (--phone)': {
        paddingRight: 0
      },
      'p:first-of-type': {
        marginTop: 0
      },
      'p:last-of-type': {
        marginBottom: 0
      },
      p: {
        position: 'relative'
      },
      'p:first-of-type:before': {
        content: '>',
        display: 'block',
        position: 'absolute',
        left: '-25px',
        color: 'accent'
      }
    },
    mark: {
      background: 'accent',
      color: 'background'
    }
  }
};

export default theme;
